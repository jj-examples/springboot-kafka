package com.example.kafkasendreceivetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaSendReceiveTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaSendReceiveTestApplication.class, args);
	}

}