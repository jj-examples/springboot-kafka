package com.example.kafkasendreceivetest.listener;

import com.google.common.collect.Lists;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Just for testing purposes
 */
@Component
@KafkaListener(topics = "${kafka.topic}",groupId = "${kafka.group}")
@Getter
@Slf4j
public class KafkaMessageListener {

    private final List<String> arrivedMessage = Lists.newLinkedList();

    @KafkaHandler
    public void handleMessage(String message){
        log.debug("msg received: {}",message);
        arrivedMessage.add(message);
    }

    public void resetResult(){
        arrivedMessage.clear();
    }


}
