package com.example.kafkasendreceivetest.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageSenderService {

    private final KafkaTemplate<String,String> kafkaTemple;
    private final String topicName;

    public MessageSenderService(KafkaTemplate<String, String> kafkaTemple,
                                @Value(value = "${kafka.topic}") String topicName) {
        this.kafkaTemple = kafkaTemple;
        this.topicName = topicName;
    }

    public void send(String message){
        kafkaTemple.send(topicName,message)
            .whenCompleteAsync((result,exception)->{
                if(exception == null){
                    log.debug("msg : {} sent",message);
                }else {
                   log.error(exception.getMessage());
                }
            });
    }
}
