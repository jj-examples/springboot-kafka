package com.example.kafkasendreceivetest.rest.controller;

import com.example.kafkasendreceivetest.rest.dto.UserInfo;
import com.example.kafkasendreceivetest.service.MessageSenderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserInfoController {

    private final MessageSenderService messageSenderService;

    private final ObjectMapper objectMapper;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> receiveUserInfo(@Valid @RequestBody UserInfo userInfo) throws JsonProcessingException {
        messageSenderService.send(objectMapper.writeValueAsString(userInfo));
        return ResponseEntity.ok().build();
    }
}
