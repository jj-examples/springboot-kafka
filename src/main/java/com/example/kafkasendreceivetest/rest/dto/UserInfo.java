package com.example.kafkasendreceivetest.rest.dto;

import java.io.Serializable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record UserInfo(@NotBlank String firstName,
                       @NotBlank String lastName,
                       @NotNull Integer age) implements Serializable {
}
