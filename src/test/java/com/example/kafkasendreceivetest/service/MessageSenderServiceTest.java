package com.example.kafkasendreceivetest.service;

import com.example.kafkasendreceivetest.IntegrationTest;

import org.awaitility.Awaitility;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MessageSenderServiceTest extends IntegrationTest {

    @Autowired
    private MessageSenderService messageSenderService;


    @Test
    public void sendMessage_msgProvided_msg_received(){
        int totalMessageCount = 100;
        for (int i = 0; i < totalMessageCount; i++) {
            messageSenderService.send("msg-" + i);
        }

        Awaitility
            .await()
            .forever()
            .until(() -> getKafkaMessageListener().getArrivedMessage().size(),
                   Matchers.equalTo(totalMessageCount));
    }

}
