package com.example.kafkasendreceivetest.rest;

import com.example.kafkasendreceivetest.IntegrationTest;
import com.example.kafkasendreceivetest.listener.KafkaMessageListener;
import com.example.kafkasendreceivetest.rest.dto.UserInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.awaitility.Awaitility;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

public class UserInfoControllerTest extends IntegrationTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void receiveUserInfo_whenInfoProvided_succeeded()
        throws URISyntaxException, JsonProcessingException {
        var userInfo = new UserInfo("Neel","Maniac",14);
        var expectedMessage = getObjectMapper().writeValueAsString(userInfo);
        sendRequest(userInfo);
        Awaitility.await()
            .atMost(10, TimeUnit.SECONDS)
            .until(() -> getKafkaMessageListener().getArrivedMessage().get(0),
                   Matchers.equalTo(expectedMessage));
    }


    private void sendRequest(UserInfo userInfo) throws JsonProcessingException, URISyntaxException {
        restTemplate.postForEntity("http://localhost:" + getPort() + "/user",
                              userInfo,Void.class);
    }
}
