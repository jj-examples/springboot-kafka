package com.example.kafkasendreceivetest;


import com.google.j2objc.annotations.AutoreleasePool;

import com.example.kafkasendreceivetest.config.CommonTestConfiguration;
import com.example.kafkasendreceivetest.config.KafkaTestConfiguration;
import com.example.kafkasendreceivetest.listener.KafkaMessageListener;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.test.context.ContextConfiguration;

import lombok.Getter;

@Getter
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(name = "integration-test-with-test-containers",
    classes = {KafkaTestConfiguration.class, CommonTestConfiguration.class})
public abstract class IntegrationTest {

    @Value(value="${local.server.port}")
    private int port;

    @Autowired
    KafkaMessageListener kafkaMessageListener;

    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    public void beforeTest(){
        kafkaMessageListener.resetResult();
    }


}
